<?php
if( function_exists('acf_add_local_field_group') ):
$module = "bike";
acf_add_local_field_group(array (
	'key' => create_key($module),
	'title' => 'Test',
	'fields' => array (
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'bike',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
?>