<?php
	require get_template_directory() . '/includes/post_types/bike/bike.class.php';
	require get_template_directory() . '/includes/post_types/bike/cpt-acf.php';
	require get_template_directory() . '/includes/post_types/bike/cpt-def.php';

function uap_bike( $bike_ref = null ){
	$bike = new UAP\bike( $bike_ref );
	return $bike;
}

function get_all_bikes( $args = array() ){
	$bikes = array();
	$defaults = array(
		'posts_per_page' => 2,
		'paged' => '1',
	);
	$params = array_replace_recursive($defaults, $args);

	$args=array(
		'post_type' => 'bike',
		'post_status' => 'publish',
		'posts_per_page' => $params['posts_per_page'],
		'paged' => $params['paged']
	);
	if( array_key_exists( 'orderby' , $params) ){
		$args['orderby'] = $params['orderby'];
		$args['order'] = 'ASC';
	}
	if( array_key_exists( 'subject' , $params) && $params['subject'] ){
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'bike_cat',
				'field'    => 'term_id',
				'terms'    => $params['subject'],
			),
		);
	}
	if( array_key_exists('meta_query',$params)){
		foreach( $params['meta_query'] as $mq_arr ){
			$args['meta_query'][] = array(
				'key'     => $mq_arr['key'],
				'value'   => $mq_arr['value'],
				'compare' => $mq_arr['compare'],
			);
		}
	}
	$bike_query = new WP_Query($args);
	if( $bike_query->have_posts() ):
		foreach($bike_query->posts as $bike_post):
			$bikes[] = new UAP\Bike($bike_post);			
		endforeach;
	endif;
	return $bikes;
}

// AJAX CALL ON CATALOG PAGE
add_action( 'wp_ajax_nopriv_get_ajax_bikes', 'get_ajax_bikes' );
add_action( 'wp_ajax_get_ajax_bikes', 'get_ajax_bikes' );
function get_ajax_bikes(){
	$params = $_REQUEST['params'];
	$param = array(
		'post_type' => 'bike', 
		'posts_per_page' => $params['posts_per_page'], 
		'paged' => $params['paged'], 
		'orderby' => $params['orderby'],
		'subject' => $params['subject']
	);
	$bikes = get_all_bikes( $param );
	$result = json_decode(json_encode($bikes), true);
	$response['bikes'] = $result;
	echo json_encode( $response );
	exit;
}
function get_default_bike_cover(){
	$image = get_field('default_bike_cover','option');
	return $image['url'];
}