<?php
namespace UAP;
class Bike{
	public function __construct( $bike_ref = null ){
		global $pilot;
		if( is_numeric( $bike_ref ) ){
			$bike = get_bike_by( 'ID', $bike_ref );
		}
		elseif( is_object( $bike_ref ) ){
			$bike = $bike_ref;
		}
		else{
			$bike = "";
		}
		if( is_object( $bike ) ){
			foreach( $bike as $key => $value ){
				$this->$key = $value;
			}
		}
		$this->get_bike_meta();
		$this->wp_bike = $bike;
	}
	public function get_bike_meta(){
		$this->cover = get_field('bike_cover', $this->ID );
		$this->description = get_field('bike_briefdescription', $this->ID );
		$this->excerpt = get_field('bike_excerpt', $this->ID );
		$this->subtitle = get_field('bike_subtitle', $this->ID );
		$this->permalink = get_permalink($this->ID);
		$this->authors = get_field('bike_author', $this->ID );
		$this->editors = get_field('bike_editor', $this->ID );
		$this->pages = get_field('bike_pagecount', $this->ID );
		$this->trimsize = get_field('bike_trimsize', $this->ID );
		$this->isbn = get_field('bike_isbn', $this->ID );
		$this->pubdate = get_field('bike_pubdate', $this->ID );

		$this->awards = get_field('bike_awards', $this->ID );
		$this->news = get_field('bike_news', $this->ID );
		$this->events = get_field('bike_events', $this->ID );

		// CATEGORIES
		$this->related = get_the_terms( $this->ID,'bike_cat' ); 

		//QUOTES
		$this->quotes = [];
		if( $quote = get_field('bike_quoteone', $this->ID ) ){
			$this->quotes[] = $quote;
		}
		if( $quote = get_field('bike_quotetwo', $this->ID ) ){
			$this->quotes[] = $quote;
		}
		if( $quote = get_field('bike_quotethree', $this->ID ) ){
			$this->quotes[] = $quote;
		}

		//SERIES
		$this->series = [];
		$series_array = get_posts(array(
			'post_type' => 'series',
			'meta_query' => array(
				array(
					'key' => 'series_bikes', // name of custom field
					'value' => '"' . $this->ID . '"', 
					'compare' => 'LIKE'
				)
			)
		));
		foreach( $series_array as $series ){
			$series->permalink = get_permalink( $series->ID); 
			$this->series[] = $series;
		}

		//RELATED NEWS 
		$this->news = [];
		$news_array = get_posts(array(
			'post_type' => 'post',
			'meta_query' => array(
				array(
					'key' => 'related_bikes', // name of custom field
					'value' => '"' . $this->ID . '"', 
					'compare' => 'LIKE'
				)
			)
		));
		foreach( $news_array as $news ){
			$news->permalink = get_permalink( $news->ID); 
			$this->news[] = $news;
		}

		//RELATED EVENTS 
		$this->events = [];
		$events_array = get_posts(array(
			'post_type' => 'event',
			'meta_query' => array(
				array(
					'key' => 'related_bikes', // name of custom field
					'value' => '"' . $this->ID . '"', 
					'compare' => 'LIKE'
				)
			)
		));
		foreach( $events_array as $event ){
			$event->permalink = get_permalink( $event->ID); 
			$this->events[] = $event;
		}

	}

}