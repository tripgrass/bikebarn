<?php
/**
 * Bike Custom Post Type
 */
add_action( 'init', 'register_bike_post_type');
function register_bike_post_type()
{ 
	register_post_type( 'bike',
		array( 'labels' => 
			array(
				'name'               => 'Bikes',
				'singular_name'      => 'Bike',
				'all_items'          => 'All Bikes',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Bike',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Bike',
				'new_item'           => 'New Bike',
				'view_item'          => 'View Bike',
				'search_items'       => 'Search Bikes',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Bikes post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 13,
			'supports'      	=> array( 'title', 'thumbnail', 'author' ),
			'menu_icon'           => 'dashicons-admin-generic',
			 'rewrite'	      => array( 'slug' => 'bike', 'with_front' => false ),
			 'has_archive'      => 'bikes',
		)
	);
}
/**
 * Author Categories
 */
register_taxonomy( 'bike_cat', 
	array('bike'),
	array('hierarchical' => true,
		'labels' => array(
			'name' 				=> 'Subjects',
			'singular_name' 	=> 'Subjects',
			'search_items' 		=> 'Search Subjects',
			'all_items' 		=> 'All Subjects',
			'parent_item' 		=> 'Parent Subject',
			'parent_item_colon' => 'Parent Subject:',
			'edit_item' 		=> 'Edit Subject',
			'update_item' 		=> 'Update Chaper',
			'add_new_item' 		=> 'Add New Subject',
			'new_item_name' 	=> 'New Subject Name',
		),
		'show_admin_column' => true, 
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'rewrite' 			=> array( 'slug' => 'bike-cat' ),
	)
);

?>