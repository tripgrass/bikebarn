=== Pilot ===

Contributors: sonder agency
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments

Requires at least: 4.0
Tested up to: 4.3.1
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A starter theme called Pilot.

== Description ==

Pilot is a module-based starter theme for projects on a small or medium scale.  Individual modules are 
located inside the 'includes' folder and can be added or removed as necessary.  Grunt tasks compile and
minify their components.

== Overview for Clients ==

Pilot Description and Feature List:

Our Pilot theme is designed to be a robust, user-friendly, and scalable architecture.  It operates on the idea that each part of each webpage is a separate, re-usable entity (module) that can be moved, edited, and colored with minimal effort on the part of the content provider.

We accomplish this by providing built-in modules that can be added to any page, or saved for usage across multiple pages.  By default, these modules include, but are not limited to:

o Image Carousels
o Image Galleries
o Background videos and popup videos
o Accordion content
o Table Content
o Blog Post Feeds
o Call to Action
o On-page navigation
o Generic WYSIWYG content

Each project has unique needs, and so we create and customize modules to meet user requirements.  New modules can be built and added to the site at any time without disrupting the site's functionality.  Within the theme files, we also provide documentation for other developers to edit or add their own modules.

Wordpress's administrative backend will also be customized to be simple and intuitive.  We add or reorganize features within Wordpress to allow for the revision, management, and archiving of site content.  

Pilot also includes a 'colormaker' feature.  With this, users have control over a limited number of theme colors used across the site.  Users can add new color sets and switch between color sets with the click of a button.

Behind the scenes, Pilot uses a system of file combination and minification to optimize site speed by serving as few files as possible and making those files as small as possible.  Its standardized approach to html architecture helps with Search Engine Optimization (SEO), theme maintenance, and accessibility.

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Pilot includes support for Infinite Scroll in Jetpack.

